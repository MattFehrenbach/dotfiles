#!/bin/zsh

# Default programs:
export EDITOR="nvim"
export TERMINAL="st"

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

# Config home:
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# Data home:
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"

# Cache home:
